package controller;


import dao.OrderDao;
import model.ClientOrder;
import model.OrderReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import service.OrderService;

import javax.validation.Valid;
import java.util.List;


@RestController("")
@Controller
public class OrderController {

    @Autowired
    private OrderDao orderDao;


    @GetMapping(value = "/orders", produces = "application/json")
    public List<ClientOrder> getPosts() {
        System.out.println(orderDao.findAll());
            return orderDao.findAll();
    }

    @GetMapping(value = "/orders/{id}", produces = "application/json")
    public ClientOrder getOne(@PathVariable Long id) {
        try {
            return orderDao.findOrderById(id);
        } catch (Exception e) {
            return null;
        }
    }

    @PostMapping(value = "/orders", produces = "application/json", consumes = "application/json")
    public ClientOrder saveOrder(@RequestBody @Valid ClientOrder clientOrder) {
        return orderDao.save(clientOrder);
    }

    @DeleteMapping(value = "/orders", produces = "application/json")
    public void deleteOrder() {
        orderDao.deletaAll();

    }

    @GetMapping(value = "/orders/report", produces = "application/json")
    public OrderReport getReport() {
        OrderService orderService = new OrderService();
        return orderService.getReport(getPosts());
    }


}