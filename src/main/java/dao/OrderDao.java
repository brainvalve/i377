package dao;


import model.ClientOrder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class OrderDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public ClientOrder save(ClientOrder clientOrder) {
        if (clientOrder.getId() == null) {
            em.persist(clientOrder);
        } else {
            em.merge(clientOrder);
        }
        return clientOrder;
    }

    public List<ClientOrder> findAll() {
        return em.createQuery(
                "SELECT distinct p FROM ClientOrder p JOIN FETCH p.orderRows ",
                ClientOrder.class).getResultList();

    }

    public ClientOrder findOrderById(Long name) {
        TypedQuery<ClientOrder> query = em.createQuery(
                "SELECT distinct p FROM ClientOrder p JOIN FETCH p.orderRows" +
                        " WHERE p.id = :id", ClientOrder.class);

        query.setParameter("id", name);

        return query.getSingleResult();
    }

    @Transactional
    public void deletaAll() {
        int deletedCount = em.createQuery("DELETE FROM ClientOrder").executeUpdate();
        System.out.println("Deleted " + deletedCount + " rows");
    }

}
