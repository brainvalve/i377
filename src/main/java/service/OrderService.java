package service;


import model.ClientOrder;
import model.OrderReport;
import model.OrderRow;

import java.util.List;


public class OrderService {

    private double getAverageOrderAmount(List<ClientOrder> orders) {
        double sum = 0;
        if (orders.size() == 0) {
            return 0;
        }
        for (ClientOrder order : orders) {
            for (OrderRow orderRow : order.getOrderRows()) {
                sum += orderRow.getPrice() * orderRow.getQuantity();
            }
        }
        return sum / orders.size();
    }

    private double getTurnoverWithoutVAT(List<ClientOrder> orders) {
        double sum = 0;
        for (ClientOrder order : orders) {
            for (OrderRow orderRow : order.getOrderRows()) {
                sum += orderRow.getPrice() * orderRow.getQuantity();
            }
        }
        return sum;
    }

    private double getTurnoverVAT(List<ClientOrder> orders) {
        double sum = 0;
        for (ClientOrder order : orders) {
            for (OrderRow orderRow : order.getOrderRows()) {
                sum += orderRow.getPrice() * orderRow.getQuantity();
            }
        }
        return sum * 0.2;
    }

    private double getTurnoverWithVAT(List<ClientOrder> orders) {
        double sum = 0;
        for (ClientOrder order : orders) {
            for (OrderRow orderRow : order.getOrderRows()) {
                sum += orderRow.getPrice() * orderRow.getQuantity();
            }
        }
        return sum * 1.2;
    }

    public OrderReport getReport(List<ClientOrder> orders) {
        return OrderReport
                .builder()
                .averageOrderAmount(getAverageOrderAmount(orders))
                .count(orders.size())
                .turnoverVAT(getTurnoverVAT(orders))
                .turnoverWithoutVAT(getTurnoverWithoutVAT(orders))
                .turnoverWithVAT(getTurnoverWithVAT(orders))
                .build();
    }


}