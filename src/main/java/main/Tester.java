package main;

import config.DbConfig;
import dao.OrderDao;
import model.ClientOrder;
import model.OrderRow;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Tester {

    public static void main(String[] args) {

        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(DbConfig.class);

        OrderDao dao = ctx.getBean(OrderDao.class);

        ClientOrder order = new ClientOrder();
        order.setId(1L);
        order.setOrderNumber("ordernr");

        OrderRow orderRow1 = new OrderRow();
        orderRow1.setItemName("abc");
        orderRow1.setPrice(1);
        orderRow1.setQuantity(3);

        OrderRow orderRow2 = new OrderRow();
        orderRow2.setItemName("abc");
        orderRow2.setPrice(1);
        orderRow2.setQuantity(3);

        order.getOrderRows().add(orderRow1);
        order.getOrderRows().add(orderRow2);


        dao.save(order);
        System.out.println(dao.findAll());

        ctx.close();
    }
}