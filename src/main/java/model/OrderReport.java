package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class OrderReport {

    private int count;
    private double averageOrderAmount;
    private double turnoverWithoutVAT;
    private double turnoverVAT;
    private double turnoverWithVAT;

}
