package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;

@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Data
public class OrderRow {

    @Column(name = "item_name")
    String itemName;

    @Min(value = 1, message = "quantity min value = 1 ")
    @Column(name = "quantity")
    int quantity;

    @Min(value = 1, message = "price min value = 1")
    @Column(name = "price")
    int price;


}
