package model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "clientOrder")
public class ClientOrder {

    public ClientOrder(Long id) {
        this.id = id;
    }

    public ClientOrder() {
    }

    @Id
    @SequenceGenerator(name = "order_sequence", sequenceName = "order_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_sequence")
    Long id;

    @Length(min = 2)
    @Column(name = "order_number")
    private String orderNumber;

    @ElementCollection
    @CollectionTable(
            name = "order_rows",
            joinColumns = @JoinColumn(name = "orders_id", referencedColumnName = "id")
    )
    @Valid
    private List<OrderRow> orderRows = new ArrayList<>();

}
